#!/bin/sh

PROJ_DIR=/home/adinasarapu/Projects/WGS_TCA21177/DBA2J_isec

module load samtools/1.3
module load vcftools/0.1.15
module load bcftools/1.3

for i in {929..939}; do

        SID=SL151${i}
        VCF_IN="${PROJ_DIR}/${SID}/0002.vcf"
        VCF_OUT="${PROJ_DIR}/${SID}/0002.vcf.gz"

        cat ${VCF_IN} | bgzip -c > ${VCF_OUT}   
        tabix -p vcf ${VCF_OUT}

	bcftools query \
		-f '%CHROM\t%POS\t%REF\t%ALT[\t%SAMPLE=%GT]\n' \
		${VCF_OUT} -o ${PROJ_DIR}/${SID}_0002.tab
done

# %CHROM	The CHROM column (similarly also other columns, such as POS, ID, QUAL, etc.)
# %INFO/TAG	Any tag in the INFO column
# %TYPE		Variant type (REF, SNP, MNP, INDEL, OTHER)
# %MASK		Indicates presence of the site in other files (with multiple files)
# %TAG{INT}	Curly brackets to subscript vectors (0-based)
# []		The brackets loop over all samples
# %GT		Genotype (e.g. 0/1)
# %TGT		Translated genotype (e.g. C/A)
# %LINE		Prints the whole line
# %SAMPLE	Sample name

module unload samtools/1.3
module unload vcftools/0.1.15
module unload bcftools/1.3
