#!/bin/sh

#===============================================#
# Merge individual sample VCF files into	# 
#	A) a multi-sample VCF (Option 1 & 2)	#
#	B) a single-sample VCF (Option 3)	#
#===============================================#

# You will need to compress the vcf files with bgzip and 
# index with tabix before you can run any vcftools functions on them.

#=======================#
# variant normalization	#
#=======================#

# http://genome.sph.umich.edu/wiki/Variant_Normalization

:<<'end1'
module load bcftools

bcftools norm \
	--multiallelics '-any' a.vcf | \
	bcftools norm -f '/path/to/genome.fa' > a.normed.vcf
bcftools norm \
        --multiallelics '-any' b.vcf | \
	bcftools norm -f '/path/to/genome.fa' > b.normed.vcf

module unload bcftools
end1
#=====================#
# Option 1: vcf-merge #
#=====================#

: <<'end2'
module load vcftools
module load samtools

vcf-merge a.normed.vcf a.normed.vcf | \
	bgzip > out.vcf.gz
bcftools index out.vcf.gz -t

module unload vcftools
module unload samtools
end2

#==========================#
# Option 2: bcftools merge #
#==========================#
# -m all: SNP records can be merged with indel records
# -Oz : compressed VCF

: <<'end3'
module load bcftools

bcftools merge \
	-m all a.normed.vcf b.normed.vcf | \
	bgzip -c > out.vcf.gz 
bcftools index out.vcf.gz -t

module unload bcftools
end3

#===============================================#
# Option 3: java -jar $GATK -T CombineVariants	#
#===============================================# 

PROJ_DIR=${HOME}/Projects/WGS_TCA21177/DBA

# IN_FILE1=${PROJ_DIR}/DBA_1J.mgp.v5.snps.dbSNP142.chr.vcf.gz
# IN_FILE2=${PROJ_DIR}/DBA_1J.mgp.v5.indels.dbSNP142.normed.chr.vcf.gz
# OUT_FILE=${PROJ_DIR}/DBA_1J.mgp.v5.snps.indels.ver2.dbSNP142.chr.vcf.gz

IN_FILE1=${PROJ_DIR}/DBA_2J.mgp.v5.snps.dbSNP142.chr.vcf.gz
IN_FILE2=${PROJ_DIR}/DBA_2J.mgp.v5.indels.dbSNP142.normed.chr.vcf.gz
OUT_FILE=${PROJ_DIR}/DBA_2J.mgp.v5.snps.indels.ver2.dbSNP142.chr.vcf.gz

REF=$HOME/iGenomes/Homo_sapiens/UCSC/hg38/Sequence/WholeGenomeFasta/genome.fa

java -jar $GATK \
	-T CombineVariants \
	--genotypemergeoption UNSORTED \
	-V ${IN_FILE1} \
	-R $REF \
	-V ${IN_FILE2} \
	-o ${OUT_FILE}
