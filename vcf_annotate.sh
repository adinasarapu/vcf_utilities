#!/bin/sh

echo "Start - `date`" 
#$ -N VCF.Anot
#$ -q all.q
##$ -l h_rt=220:00:00
#$ -pe smp 9
#$ -cwd
#$ -j y
#$ -m abe
#$ -M ashok.reddy.dinasarapu@emory.edu

# Annotate VCF file using vcf-annotate

: <<'END'

# dbSNP annotations

# From NCBI

$wget -r ftp://ftp.ncbi.nlm.nih.gov/snp/organisms/mouse_10090/VCF/

SNPs and indels are annotated with rs IDs from dbSNP Build 142. The dbSNP data was
downloaded from:

ftp://ftp.ncbi.nlm.nih.gov/snp/organisms/mouse_10090/VCF/

and the 'vcf-annotate' Perl utility from the VCFtools package (Danecek et al, 2011) was
used to add the rsIDs to calls in this release. (See below for VCFtools information).

For SNPs, the position, reference allele and alternative alleles were all compared:
e.g.
vcf-annotate -c CHROM,POS,ID,REF,ALT

For indels, only the positions were matched:
e.g.
vcf-annotate -c CHROM,POS,ID

#===========#
# From UCSC #
#===========#

wget http://hgdownload.cse.ucsc.edu/goldenPath/mm10/database/snp142.txt.gz
gunzip snp142.txt.gz
# module load samtools (vcfutils.pl is part of samtools)
vcfutils.pl ucscsnp2vcf snp142.txt | bgzip > snp142.vcf.gz
# vcf-sort -c : sort chromosomal-order 
zcat snp142.vcf.gz | vcf-sort -c | bgzip -c > snp142.sorted.vcf.gz 
tabix -p vcf snp142.sorted.vcf.gz

END

PROJ_DIR=${HOME}/Projects/WGS_TCA21177/Tamara
DB_SNP=${HOME}/Projects/WGS_TCA21177/dbSNP/snp142.sorted.vcf.gz

# samtools needed for tabix indexing
module load samtools/1.3 
module load vcftools/0.1.15
module load bcftools/1.3 

for i in {929..939}; do
	SID=SL151${i}
        VCF_IN="${PROJ_DIR}/${SID}.sorted.vcf.gz"
        VCF_OUT="${PROJ_DIR}/${SID}.dbSNP142.vcf.gz"
	bcftools annotate -c ID -a ${DB_SNP} ${VCF_IN} | bgzip -c > ${VCF_OUT}
	#zcat ${VCF_IN} | \
	#	vcf-annotate -a ${DB_SNP} \
	#	-c CHROM,POS,REF,ALT,ID,-,-,- | \
	#	bgzip -c > ${VCF_OUT} 	
	tabix -p vcf ${VCF_OUT}
done

module load bcftools/1.3
module unload vcftools/0.1.15
module unload samtools/1.3 
